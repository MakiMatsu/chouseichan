import React from 'react';
import { withRouter } from 'react-router';
import { Form, Input, Button, Typography } from 'antd';

const { Title } = Typography;
const { TextArea } = Input;

class EventEntry extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            // ここにstateの初期値を書きます
        };
    }

    registerEvent = (evt) => {
        evt.preventDefault();
        // ここにサーバーにデータを送信する処理を書きます。
    }

    onChangeEventName = (evt) => {
        // 入力された値(evt.target.value)をsetStateします
    }

    onChangeDescription = (evt) => {
    }

    onChangeCandidates = (evt) => {

    }

    // <div>内に入力フォームを作ります
    render() {
        return (
            <div>
                <Title level={2}>イベント登録</Title>
                <Form onSubmit={this.registerEvent}>

                </Form>
            </div>
        );
    }

}

export default withRouter(EventEntry);